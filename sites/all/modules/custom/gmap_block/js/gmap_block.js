/* Adding Javascript Behavior for Blocks */
Drupal.behaviors.gmap_block = {
  attach : function()
  {
    // google.maps.event.addDomListener(window, 'load',blockMap(Drupal.settings.gmap_block.latitude,Drupal.settings.gmap_block.longtitude,Drupal.settings.gmap_block.markers,Drupal.settings.gmap_block.eid,Drupal.settings.gmap_block.zoom,Drupal.settings.gmap_block.mtype,Drupal.settings.gmap_block.banimation,Drupal.settings.gmap_block.b_icon_type,Drupal.settings.gmap_block.modpath,Drupal.settings.gmap_block.b_fname,'ketan'));
    // arrayTest(Drupal.settings.gmap_block.values);
    // google.maps.event.addDomListener(window, 'load',
      blockLoop(Drupal.settings.gmap_block.latitude,Drupal.settings.gmap_block.longtitude,Drupal.settings.gmap_block.menabled,Drupal.settings.gmap_block.markers,Drupal.settings.gmap_block.eid,Drupal.settings.gmap_block.zoom,Drupal.settings.gmap_block.mtype,Drupal.settings.gmap_block.banimation,Drupal.settings.gmap_block.mdraggable,Drupal.settings.gmap_block.b_icon_type,Drupal.settings.gmap_block.modpath,Drupal.settings.gmap_block.b_fname);
  }
}

function blockLoop(latitude,longtitude,menabled,markers,eid,zoom,mtype,animation,draggable,icon_type,modpath,b_fname) {
  var b_count = latitude.length;
  for(var i = 0; i < b_count; i++) {
  google.maps.event.addDomListener(window, 'load',blockMap(latitude[i],longtitude[i],menabled[i],markers[i],eid[i],zoom[0],mtype[0],animation[0],draggable[0],icon_type[0],modpath[0],b_fname[i],i));
  }
}

/* For blocks */
function blockMap(latitude,longtitude,m_enabled,markers,eid,zoom,mtype,animation,draggable,icon_type,modpath,b_fname,serial) {

  //Options for Map such as zoom, lat & lon, map-type
  var mapOptions = {
    'zoom' : parseInt(zoom),
    center: new google.maps.LatLng(latitude,longtitude),
    'mapTypeId': eval(mtype),
  };
  
  var bmap = new google.maps.Map(document.getElementById(eid),mapOptions); //Map Object

  var tmarkers = markers.split('\n');
  var len = tmarkers.length;
  for(i = 0 ; i < len  ; i++) {
    var vals = tmarkers[i].split(',');
    var latlang = new google.maps.LatLng(vals[0],vals[1]);
    var title = vals[2];
    var choice;
  
  switch(icon_type) {
    case 'image':
      choice = '../' + modpath + '/micons/sports/' + b_fname;
      break;
    case 'star':
      choice = {
        path: 'M 125,5 155,90 245,90 175,145 200,230 125,180 50,230 75,145 5,90 95,90 z',
        fillColor: 'violet',
        fillOpacity: 0.8,
        scale: 0.1,
        strokeColor: 'red',
        strokeWeight: 1,
      };
      break;
    case 'circle':
      choice = {
        path: google.maps.SymbolPath.CIRCLE,
        fillColor: 'red',
        fillOpacity: 0.8,
        scale: 10,
        strokeColor: 'violet',
        strokeWeight: 2,
      };
      break;
  }

  //Options for Map such as zoom, lat & lon, map-type
    var markerOptions = {
      position: latlang,
      icon: choice,
      title: title,
      animation: eval(animation),
      draggable : eval(draggable),
      map : bmap,
    };

    var marker = new google.maps.Marker(markerOptions);
  }
}