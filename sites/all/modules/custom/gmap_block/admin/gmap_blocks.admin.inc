<?php
/* For handling Attach to blocks page */
function gmap_blocks_config_blocks($form, &$form_state) {
		/* Previous Value if applicable */
		$attached_blocks = variable_get('gmap_block_blocks','NULL');
		
		$block_delta = array();
		$block_lat = array();
		$block_long = array();
		$block_menabled = array();
		$block_markers = array();
		$block_target = array();

		if($attached_blocks != 'NULL') {
			foreach($attached_blocks as $blk) {
				$blk = explode("#",$blk);
				array_push($block_delta, $blk[0]);
				array_push($block_lat, $blk[1]);
				array_push($block_long, $blk[2]);
				array_push($block_menabled, $blk[3]);
				array_push($block_markers, $blk[4]);
				array_push($block_target, $blk[5]);
			}
		}

	$form = array();

	$form['wrap'] = array(
		'#type' => 'fieldset',
		'#title' => t('Configure Blocks'),
		'#collapsible' => TRUE,
		'#collapsed' => TRUE, 
		);
	
	$form['wrap']['intro'] = array(
		'#type' => 'markup',
		'#markup' => '<br/>Enable for blocks by specifying latitude and longitude value',
		);
	$blocks = gmap_blocks_list_blocks();

	foreach ($blocks as $block) {
		// dpm(print_r($block,True));

		$form['wrap'][$block->delta] = array(
			'#type' => 'checkbox',
			'#title' => $block->title . "<br/>By Module : ". $block->module . "<br/> Region : " . $block->region,
			'#return_value' => $block->delta,
			);
		
		$form['wrap'][$block->delta . '-target'] = array(
			'#type' => 'textfield',
			'#default_value' => 'map-canvas',
			'#title' => t('Target Element - ID'),
			);

		$form['wrap'][$block->delta .'-lat'] = array(
			'#type' => 'textfield',
			'#title' => 'Latitude',
			'#size' => 12,
			);

		$form['wrap'][$block->delta .'-long'] = array(
			'#type' => 'textfield',
			'#title' => 'Longitude',
			'#size' => 12,
			);

		$form['wrap'][$block->delta . '-menabled'] = array(
			'#type' => 'checkbox',
			'#title' => 'Markers',
			'#return_value' => $block->delta,
			'#default_value' => FALSE,
			);

		$form['wrap'][$block->delta . '-markers'] = array(
			'#type' => 'textarea',
			'#title' => 'Markers',
			'#description' => 'Add comma seperated values, use next line for another marker.<br/>Such as <strong>latitude,longitude,title</strong>',
			'#rows' => 5,
			);

		$form['wrap'][$block->delta .'-target']['#states'] = array(
			'visible' => array(
				':input[name=' . $block->delta .']' => array('checked' => TRUE),
				),
			);

		$form['wrap'][$block->delta .'-lat']['#states'] = array(
			'visible' => array(
				':input[name=' . $block->delta .']' => array('checked' => TRUE),
				),
			);

		$form['wrap'][$block->delta .'-long']['#states'] = array(
			'visible' => array(
				':input[name=' . $block->delta .']' => array('checked' => TRUE),
				),
			);

		$form['wrap'][$block->delta .'-long']['#states'] = array(
			'visible' => array(
				':input[name=' . $block->delta .']' => array('checked' => TRUE),
				),
			);

		$form['wrap'][$block->delta .'-menabled']['#states'] = array(
			'visible' => array(
				':input[name=' . $block->delta .']' => array('checked' => TRUE),
				),
			);

		$form['wrap'][$block->delta .'-markers']['#states'] = array(
			'visible' => array(
				':input[name=' . $block->delta .'-menabled]' => array('checked' => TRUE),
				),
			);

			if($attached_blocks != 'NULL') {
				// dvm($attached_blocks);
				$count = -1;
				$key = -1;
				
				foreach ($block_delta as $value) {
					$count++;
					if($value == $block->delta)
						$key = $count;
				}
			// }

			if($key > -1) {
			$form['wrap'][$block->delta]['#default_value'] = TRUE;
			$form['wrap'][$block->delta . '-lat']['#default_value'] = $block_lat[$key];
			$form['wrap'][$block->delta . '-long']['#default_value'] = $block_long[$key];
		
			$form['wrap'][$block->delta . '-menabled']['#default_value'] = $block_menabled[$key];

			$form['wrap'][$block->delta . '-markers']['#default_value'] = $block_markers[$key];
			$form['wrap'][$block->delta . '-target']['#default_value'] = $block_target[$key];
			}
		}

	}

	$form['submit'] = array(
		'#type' => 'submit',
		'#value' => 'Save Changes',
		);
	
	$form['reset'] = array(
		'#type' => 'markup',
		'#markup' => '<input type=reset  class=form-submit />'
		);
	return $form;
}
/* Saving the required value into VARIABLE table*/
function gmap_blocks_config_blocks_submit($form, &$form_state) {
	$attach_block = array();
	$blocks = gmap_blocks_list_blocks();
	foreach ($blocks as $block) {
		$type = $form_state['values'][$block->delta];

		if($type) {
			$bdelta = $form_state['values'][$block->delta];
			$target = $form_state['values'][$block->delta . '-target'];
			$latitude = $form_state['values'][$block->delta . '-lat'];
			$longitude = $form_state['values'][$block->delta . '-long'];
			$menabled = $form_state['values'][$block->delta . '-menabled'];

			if($menabled)
				$markers = $form_state['values'][$block->delta . '-markers'];
			else
				$markers = ' ';

			array_push($attach_block, $bdelta . '#' . $latitude . '#' . $longitude . '#' . $menabled . '#' . $markers . '#' . $target);
		}
	}
	variable_set('gmap_block_blocks',$attach_block);
	drupal_flush_all_caches();
	drupal_set_message("Settings Saved. Clear Cache in case changes are not in effect");
}

/* For listing all blocks without grouping them by region */
function gmap_blocks_list_blocks() {
  global $theme_key;

  $query = db_select('block', 'b');
  $result = $query->fields('b')->condition('b.theme', $theme_key)->condition('b.status', 1)->orderBy('b.region')->orderBy('b.weight')->orderBy('b.module')->addTag('block_load')->addTag('translatable')->execute();

  $block_info = $result->fetchAllAssoc('bid');
  // Allow modules to modify the block list.
  drupal_alter('block_list', $block_info);

  $blocks = array();
  foreach ($block_info as $block) {
    $blocks["{$block->module}_{$block->delta}"] = $block;
  }
  // error_log(print_r($blocks,TRUE));
  return $blocks;
}